import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { BreadcumbComponent } from './shared/breadcumb/breadcumb.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { LoginComponent } from './shared/login/login.component';
import { NotfoundComponent } from './shared/notfound/notfound.component';
import { DashboardComponent } from './shared/dashboard/dashboard.component';
import { LineChartComponent } from './graficos/line-chart/line-chart.component';
import { DonoughtChartComponent } from './graficos/donought-chart/donought-chart.component';


@NgModule({
  declarations: [
    AppComponent,
    BreadcumbComponent,
    SidebarComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    NotfoundComponent,
    DashboardComponent,
    LineChartComponent,
    DonoughtChartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
